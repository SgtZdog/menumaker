﻿namespace MenuModels
{
    /// <summary>
    ///     Parent class for recipe objects
    /// </summary>
    public abstract class Recipe
    {
        /// <summary>
        ///     Used by views
        /// </summary>
        protected Recipe () { }

        /// <summary>
        ///     Used for construction from db
        /// </summary>
        /// <param name="name"></param>
        /// <param name="directions"></param>
        /// <param name="description"></param>
        /// <param name="id"></param>
        protected Recipe (string name, string directions, string description, int id)
        {
            ID = id;
            Name = name;
            Directions = directions;
            Description = description;
        }

        /// <summary>
        ///     used for conversion between child types
        /// </summary>
        /// <param name="recipe"></param>
        protected Recipe (Recipe recipe)
        {
            ID = recipe.ID;
            Name = recipe.Name;
            Directions = recipe.Directions;
            Description = recipe.Description;
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string Directions { get; set; }
        public string Description { get; set; }
    }
}