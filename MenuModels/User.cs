﻿// ReSharper disable MemberCanBeProtected.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace MenuModels
{
    /// <summary>
    ///     Parent class for user objects
    /// </summary>
    public abstract class User
    {
        /// <summary>
        ///     Used for Views
        /// </summary>
        protected User () { }

        /// <summary>
        ///     Used for construction from database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="roleId"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="roleName"></param>
        /// <param name="roleDescription"></param>
        protected User (int id, int roleId, string username, string password, string roleName, string roleDescription)
        {
            ID = id;
            RoleID = roleId;
            Username = username;
            Password = password;
            RoleName = roleName;
            RoleDescription = roleDescription;
        }

        /// <summary>
        ///     used for conversion between child types
        /// </summary>
        /// <param name="user"></param>
        protected User (User user)
        {
            ID = user.ID;
            RoleID = user.RoleID;
            Username = user.Username;
            Password = user.Password;
            RoleName = user.RoleName;
            RoleDescription = user.RoleDescription;
        }

        public int ID { get; set; }
        public int RoleID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
    }
}