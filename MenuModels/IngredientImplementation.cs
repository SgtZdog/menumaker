﻿namespace MenuModels
{
    /// <summary>
    ///     Parent class for Ingredient Implementations
    /// </summary>
    public abstract class IngredientImplementation : Ingredient
    {
        /// <summary>
        ///     used for views
        /// </summary>
        protected IngredientImplementation () { }

        /// <summary>
        ///     used for creation from db
        /// </summary>
        /// <param name="recipeId"></param>
        /// <param name="quantity"></param>
        /// <param name="ingredient"></param>
        /// <param name="implementationId"></param>
        protected IngredientImplementation (
            int recipeId,
            double quantity,
            Ingredient ingredient,
            int implementationId)
            : base(ingredient)
        {
            RecipeID = recipeId;
            ImplementationID = implementationId;
            Quantity = quantity;
        }

        /// <summary>
        ///     Used for conversion between child classes
        /// </summary>
        /// <param name="ingredientImplementation"></param>
        protected IngredientImplementation (IngredientImplementation ingredientImplementation)
            : base(ingredientImplementation)
        {
            RecipeID = ingredientImplementation.RecipeID;
            ImplementationID = ingredientImplementation.ImplementationID;
            Quantity = ingredientImplementation.Quantity;
        }

        public int RecipeID { get; set; }
        public int ImplementationID { get; set; }
        public double Quantity { get; set; }
        public double Cost => Price * Quantity;
    }
}