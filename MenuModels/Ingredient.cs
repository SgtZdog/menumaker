﻿namespace MenuModels
{
    /// <summary>
    ///     Parent class for Ingredient objects
    /// </summary>
    public abstract class Ingredient
    {
        /// <summary>
        ///     used for views
        /// </summary>
        protected Ingredient () { }

        /// <summary>
        ///     used for creation from db
        /// </summary>
        /// <param name="name"></param>
        /// <param name="unit"></param>
        /// <param name="price"></param>
        /// <param name="ingredientId"></param>
        protected Ingredient (string name, string unit, double price, int ingredientId)
        {
            IngredientID = ingredientId;
            Name = name;
            Unit = unit;
            Price = price;
        }

        /// <summary>
        ///     used for conversion between child objects
        /// </summary>
        /// <param name="ingredient"></param>
        protected Ingredient (Ingredient ingredient)
        {
            IngredientID = ingredient.IngredientID;
            Name = ingredient.Name;
            Unit = ingredient.Unit;
            Price = ingredient.Price;
        }

        public int IngredientID { get; set; }
        public string Name { get; set; }
        public string Unit { get; set; }
        public double Price { get; set; }
    }
}