﻿using System;
using MenuModels;

namespace MenuMakerBLL.Models
{
    /// <summary>
    ///     BO version of IngredientImplementation, currently unused since the only BL method operates on a list of
    ///     Parent Objects
    /// </summary>
    public class IngredientImplementationBO : IngredientImplementation
    {
        public IngredientImplementationBO (
            IngredientImplementation ingredient)
            : base(ingredient) { }

        public static IngredientImplementationBO operator + (
            IngredientImplementationBO firstIngredient,
            IngredientImplementation secondIngredient)
        {
            var result = new IngredientImplementationBO(firstIngredient);
            if (firstIngredient.IngredientID != secondIngredient.IngredientID)
                throw new ArgumentException("Ingredients don't have the same IngredientID, can't be added.");

            result.Quantity += secondIngredient.Quantity;

            return result;
        }
    }
}