﻿using System.Collections.Generic;
using MenuModels;

namespace MenuMakerBLL
{
    /// <summary>
    ///     Business Logic Object for Recipe info.
    /// </summary>
    public static class RecipeBLO
    {
        /// <summary>
        ///     Calculates the total cost of a recipe.
        ///     DEPRECATED: replace with IngredientImplementationBLO.SumIngredientsCost()
        /// </summary>
        /// <param name="ingredients">List of ingredients from the recipe.</param>
        /// <returns></returns>
        public static double CalculateTotalCost (IEnumerable<IngredientImplementation> ingredients)
        {
            return IngredientImplementationBLO.SumIngredientsCost(ingredients);
        }
    }
}