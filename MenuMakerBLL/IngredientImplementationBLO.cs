using System.Collections.Generic;
using System.Linq;
using MenuMakerBLL.Models;
using MenuModels;

namespace MenuMakerBLL
{
    /// <summary>
    ///     Ingredient Implementation Business Logic.
    /// </summary>
    public static class IngredientImplementationBLO
    {
        /// <summary>
        ///     Used for consolidating ingredient implementations to avoid duplicates of the same base ingredient.
        ///     IngredientImplementations in return will have a recipeID of the first implementation for that ingredient.
        /// </summary>
        /// <param name="ingredients"></param>
        /// <returns>List of IngredientImplementationBO</returns>
        public static IEnumerable<IngredientImplementationBO> ConsolidateIngredients (
            IEnumerable<IngredientImplementation> ingredients)
        {
            var result = new List<IngredientImplementationBO>();

            foreach (var ingredient in ingredients)
            {
                var added = false;
                for (var Index = 0; Index < result.Count; Index++)
                {
                    if (result[Index].IngredientID == ingredient.IngredientID)
                    {
                        result[Index] = result[Index] + ingredient;
                        added = true;
                        break;
                    }
                }

                if (!added) result.Add(new IngredientImplementationBO(ingredient));
            }

            return result;
        }

        /// <summary>
        ///     Calculates the total cost of ingredients
        /// </summary>
        /// <param name="ingredients">IEnumerable of ingredients.</param>
        /// <returns>Sum of cost properties</returns>
        public static double SumIngredientsCost (IEnumerable<IngredientImplementation> ingredients)
        {
            var total = ingredients.Sum(ingredient => ingredient.Cost);

            return total;
        }
    }
}