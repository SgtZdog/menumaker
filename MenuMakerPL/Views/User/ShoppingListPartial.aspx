<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage"%>
<%@ Import Namespace="MenuMakerPL.Models" %>
<% var model = (FullShoppingListVM)Session["ShoppingList"]; %>
<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="http://www.w3.org/1999/html" lang="en">
<head runat="server">
    <title>Shopping List</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-4">
            <h3>Recipes in list</h3>
            <% foreach (var recipe in model.Recipes)
               { %>
                <div class="row">
                    <div class="col-sm-6 col-xs-6"><%: recipe.Name %></div>
                    <div class="col-sm-2 col-xs-2">
                        <input class="btn btn-danger" type="button" value="Delete Recipe"
                               onclick="location.href='<%: Url.Action("DeleteShoppingListRecipe",
                                                                      "User",
                                                                      new {recipeID = recipe.ID},
                                                                      null) %>'">
                    </div>
                </div>
            <% } %>
            <input class="btn btn-danger" type="button" value="Clear Shopping List"
                   onclick="location.href='<%: Url.Action("DeleteAllShoppingListRecipes", "User") %>'">
        </div>
        <div class="col-sm-4">
            <h3>Ingredients in list</h3>
            <% foreach (var ingredient in model.Ingredients)
               { %>
                <div class="row">
                    <div class="col-sm-6 col-xs-6"><%: ingredient.Name %></div>
                    <div class="col-sm-2 col-xs-2"><%: $"{ingredient.Cost:C}" %></div>
                </div>
            <% } %>
            <div class="row">
                <h4>Total Cost:</h4><%: $"{model.Cost:C}" %>
            </div>
        </div>
    </div>
</div>
</body>
</html>