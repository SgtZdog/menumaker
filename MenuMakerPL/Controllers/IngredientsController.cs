﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;
using MenuMakerDAL;
using MenuMakerPL.Controllers.ActionFilters;
using MenuMakerPL.Models;

namespace MenuMakerPL.Controllers
{
    /// <summary>
    ///     Controller for all Ingredient details needs. Must be a registered user to view
    /// </summary>
    [RoleCheck(DefaultRedirect, StandardUser, PowerUser, Admin)]
    public class IngredientsController : MMController
    {
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        //Override DAO to clarify its type
        private IngredientsDAO _dataAccess;

        public IngredientsController ()
        {
            var connection = ConfigurationManager.ConnectionStrings["MenuMaker"].ConnectionString;
            _dataAccess = new IngredientsDAO(connection);
        }

        /// <summary>
        ///     Sets landing page to View All
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index ()
        {
            return RedirectToAction("ViewAllIngredients");
        }

        /// <summary>
        ///     Create ingredient page, must be at least PowerUser to view
        /// </summary>
        /// <returns></returns>
        [HttpGet, RoleCheck(DefaultRedirect, PowerUser, Admin)]
        public ActionResult CreateIngredient ()
        {
            //Return variable
            ActionResult result = View();

            return result;
        }

        /// <summary>
        ///     Create ingredient POST method, must be at least PowerUser to use
        /// </summary>
        /// <param name="ingredientPOToCreate">ingredient to create</param>
        /// <returns>Returns user to View All</returns>
        [HttpPost, RoleCheck(DefaultRedirect, PowerUser, Admin)]
        public ActionResult CreateIngredient (IngredientPO ingredientPOToCreate)
        {
            //Return variable
            ActionResult result;
            if (ModelState.IsValid)
                try
                {
                    _dataAccess.CreateIngredient(ingredientPOToCreate);
                    result = RedirectToAction("ViewAllIngredients");
                }
                catch (Exception e)
                {
                    //Problem occurred, return to user with message.
                    WriteErrorToLog(e);
                    ViewBag.Message = e.Data["Problem"];
                    result = View(ingredientPOToCreate);
                }
            else //ModelState not valid, return back to user.
                result = View(ingredientPOToCreate);

            return result;
        }

        /// <summary>
        ///     View All page, uses controller's access for role check
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ViewAllIngredients ()
        {
            //Return Variable
            ActionResult result;
            ViewBag.Message = TempData["Message"];
            try
            {
                var ingredients = new List<IngredientPO>();
                //Convert objects to PO for viewing
                foreach (var ingredient in _dataAccess.ReadAllIngredients())
                    ingredients.Add(new IngredientPO(ingredient));
                result = View(ingredients);
            }
            catch (Exception e)
            {
                //Problem occurred, return view with message.
                WriteErrorToLog(e);
                ViewBag.Message = e.Data["Problem"];
                result = View(new List<IngredientPO>());
            }

            return result;
        }

        /// <summary>
        ///     Get call that gets ingredient object fresh from DB
        /// </summary>
        /// <param name="ingredientID">Primary key of the Ingredient in the DB</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ViewSingleIngredient (int ingredientID)
        {
            //Return variable
            ActionResult result;
            try
            {
                //Get ingredient to view and convert to PO
                var ingredientPO = new IngredientPO(_dataAccess.ReadIngredient(ingredientID));
                result = View(ingredientPO);
                ViewBag.Message = TempData["Message"];
            }
            catch (Exception e)
            {
                //Problem occurred, return view to user with message
                WriteErrorToLog(e);
                TempData["Message"] = e.Data["Problem"];
                result = RedirectToAction("ViewAllIngredients");
            }

            return result;
        }

        /// <summary>
        ///     Update Ingredient page, only accessible to at least PowerUsers
        /// </summary>
        /// <param name="ingredientId">ID of ingredient to update</param>
        /// <returns></returns>
        [HttpGet, RoleCheck(DefaultRedirect, PowerUser, Admin)]
        public ActionResult UpdateIngredient (int ingredientId)
        {
            //Return variable
            ActionResult result;
            try
            {
                //Get ingredient and convert to PO for View
                var ingredientPO = new IngredientPO(_dataAccess.ReadIngredient(ingredientId));
                result = View(ingredientPO);
            }
            catch (Exception e)
            {
                //Problem occurred, return view to user with message
                WriteErrorToLog(e);
                ViewBag.Message = e.Data["Problem"];
                result = View(new IngredientPO());
            }

            return result;
        }

        /// <summary>
        ///     Collect form and update ingredient, returns to View Single Ingredient
        /// </summary>
        /// <param name="ingredient"></param>
        /// <returns>Single Ingredient Page</returns>
        [HttpPost, RoleCheck(DefaultRedirect, PowerUser, Admin)]
        public ActionResult UpdateIngredient (IngredientPO ingredient)
        {
            //Return Variable
            ActionResult result;
            if (ModelState.IsValid)
                try
                {
                    _dataAccess.UpdateIngredient(ingredient);
                    result = RedirectToAction("ViewSingleIngredient", new {ingredientID = ingredient.IngredientID});
                }
                catch (Exception e)
                {
                    //Error occurred, return to user with message.
                    WriteErrorToLog(e);
                    ViewBag.Message = e.Data["Problem"];
                    result = View(ingredient);
                }
            else //ModelState not valid, return to user
                result = View(ingredient);

            return result;
        }

        /// <summary>
        ///     Delete ingredient by ID, returns to View All
        /// </summary>
        /// <param name="ingredientID">id of ingredient to delete</param>
        /// <returns></returns>
        [RoleCheck(DefaultRedirect, PowerUser, Admin)]
        public ActionResult DeleteIngredient (int ingredientID)
        {
            ActionResult result;
            try
            {
                _dataAccess.DeleteIngredient(ingredientID);
                result = RedirectToAction("ViewAllIngredients");
            }
            catch (Exception e)
            {
                //Error occurred, return back to details with message
                WriteErrorToLog(e);
                TempData["Message"] = e.Data["Problem"];
                result = RedirectToAction("ViewSingleIngredient", new {ingredientID});
            }

            return result;
        }
    }
}