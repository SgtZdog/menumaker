﻿// ReSharper disable FieldCanBeMadeReadOnly.Local

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using Logger;
using MenuMakerBLL;
using MenuMakerDAL;
using MenuMakerPL.Controllers.ActionFilters;
using MenuMakerPL.Models;

namespace MenuMakerPL.Controllers
{
    /// <summary>
    ///     Recipe controller. Handles Recipes and their implementations
    ///     Uses its own DAOs instead of the parent DAO
    ///     Must be a registered user to access any content
    /// </summary>
    [RoleCheck(DefaultRedirect, StandardUser, PowerUser, Admin)]
    public class RecipeController : MMController
    {
        private IngredientImplementationDAO _ingredientImplementationDAO;
        private IngredientsDAO _ingredientsDAO;
        private RecipeDAO _recipeDAO;

        public RecipeController ()
        {
            var connection = ConfigurationManager.ConnectionStrings["MenuMaker"].ConnectionString;
            _ingredientsDAO = new IngredientsDAO(connection);
            _ingredientImplementationDAO = new IngredientImplementationDAO(connection);
            _recipeDAO = new RecipeDAO(connection);
        }

        /// <summary>
        ///     Landing page is View All Recipes
        /// </summary>
        /// <returns></returns>
        public ActionResult Index ()
        {
            return RedirectToAction("ViewAllRecipes");
        }

        /// <summary>
        ///     View all Recipes page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ViewAllRecipes ()
        {
            var recipes = new List<RecipePO>();
            //Return Variable
            ActionResult response;
            ViewBag.Message = TempData["Message"];

            try
            {
                //convert recipes to PO versions
                recipes.AddRange(_recipeDAO.ReadAllRecipes().Select(recipe => new RecipePO(recipe)));
            }
            catch (Exception e)
            {
                //Error occurred return to user with message
                WriteErrorToLog(e);
                ViewBag.Message = e.Data["Problem"];
            }
            finally
            {
                response = View(recipes);
            }

            return response;
        }

        /// <summary>
        ///     Create Recipe page, must be at least PowerUser
        /// </summary>
        /// <returns></returns>
        [HttpGet, RoleCheck(DefaultRedirect, PowerUser, Admin)]
        public ActionResult CreateRecipe ()
        {
            //Return variable
            ActionResult response = View();

            return response;
        }

        /// <summary>
        ///     Creates recipe with basic details, no ingredients and redirects to UpdateRecipe
        /// </summary>
        /// <param name="recipePO">recipe that will be created</param>
        /// <returns>Redirects to UpdateRecipe</returns>
        [HttpPost, RoleCheck(DefaultRedirect, PowerUser, Admin)]
        public ActionResult CreateRecipe (RecipePO recipePO)
        {
            //Return variable
            ActionResult response;
            if (ModelState.IsValid)
                try
                {
                    //Generate recipe and get back ID
                    response = RedirectToAction("UpdateRecipe", _recipeDAO.CreateRecipe(recipePO));
                }
                catch (Exception e)
                {
                    //Error occurred, return page with message
                    WriteErrorToLog(e);
                    ViewBag.Message = e.Data["Problem"];
                    response = View(recipePO);
                }
            else //ModelState is not valid
                response = View(recipePO);

            return response;
        }

        /// <summary>
        ///     Views single recipe with details  by ID.
        /// </summary>
        /// <param name="recipeID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ViewRecipe (int recipeID)
        {
            //Return variable
            ActionResult response;

            try
            {
                var recipePO = new RecipePO(_recipeDAO.ReadRecipe(recipeID));
                var ingredients = _ingredientImplementationDAO.ReadRecipeIngredients(recipeID);
                var cost = IngredientImplementationBLO.SumIngredientsCost(ingredients);

                //Construct VM from ingredients, recipe, and cost
                var recipeVM = new FullRecipeVM(recipePO, ingredients, cost);
                ViewBag.Message = TempData["Message"];
                response = View(recipeVM);
            }
            catch (Exception e)
            {
                //Error occurred, return view with message
                WriteErrorToLog(e);
                TempData["Message"] = e.Data["Problem"];
                response = RedirectToAction("ViewAllRecipes");
            }

            return response;
        }

        /// <summary>
        ///     Update recipe page, constructed of many partial views using ajax
        /// </summary>
        /// <param name="recipeID">ID of recipe to be updated</param>
        /// <returns></returns>
        [HttpGet, RoleCheck(DefaultRedirect, PowerUser, Admin)]
        public ActionResult UpdateRecipe (int recipeID)
        {
            //Return variable
            ActionResult response;

            try
            {
                var recipePO = new RecipePO(_recipeDAO.ReadRecipe(recipeID));
                var ingredients = _ingredientImplementationDAO.ReadRecipeIngredients(recipeID);
                response = View(new FullRecipeVM(recipePO, ingredients));
            }
            catch (Exception e)
            {
                //Error occurred, return with message.
                WriteErrorToLog(e);
                ViewBag.Message = e.Data["Problem"];
                response = View(new FullRecipeVM());
            }

            return response;
        }

        /// <summary>
        ///     Delete recipe by id, db should handle cascades. Redirects back to View All
        /// </summary>
        /// <param name="recipeID"></param>
        /// <returns>Redirect back to View All</returns>
        [RoleCheck(DefaultRedirect, PowerUser, Admin)]
        public ActionResult DeleteRecipe (int recipeID)
        {
            //Return variable
            ActionResult result;
            try
            {
                _recipeDAO.DeleteRecipe(recipeID);
                result = RedirectToAction("ViewAllRecipes");
            }
            catch (Exception e)
            {
                //Error occurred, return view with message
                WriteErrorToLog(e);
                TempData["Message"] = e.Data["Problem"];
                result = RedirectToAction("ViewRecipe", new {recipeID});
            }

            return result;
        }

        /// <summary>
        ///     Edit recipe basic details partial view
        /// </summary>
        /// <param name="recipeID">Id for recipe</param>
        /// <returns>partial view for recipe basic info update</returns>
        [HttpGet, RoleCheck(DefaultRedirect, PowerUser, Admin)]
        public PartialViewResult EditRecipePartial (int recipeID)
        {
            PartialViewResult response;
            try
            {
                ViewBag.Message = TempData["Message"];
                response = PartialView(new RecipePO(_recipeDAO.ReadRecipe(recipeID)));
            }
            catch (Exception e)
            {
                //Error occurred, return with message
                WriteErrorToLog(e);
                ViewBag.Message = e.Data["Problem"];
                response = PartialView(new RecipePO());
            }

            return response;
        }

        /// <summary>
        ///     Update recipe basic info, takes in basic details only
        /// </summary>
        /// <param name="recipe">recipe being updated</param>
        /// <returns>returns the get so that further updates can be made if needed.</returns>
        [HttpPost, RoleCheck(DefaultRedirect, PowerUser, Admin)]
        public ActionResult EditRecipePartial (RecipePO recipe)
        {
            //Return variable
            ActionResult response;
            if (ModelState.IsValid)
                try
                {
                    _recipeDAO.UpdateRecipe(recipe);
                }
                catch (Exception e)
                {
                    //Error occurred, return with message
                    WriteErrorToLog(e);
                    TempData["Message"] = e.Data["Problem"];
                }
                finally
                {
                    //Both success and fail redirect back to GET
                    response = RedirectToAction("EditRecipePartial", new {recipeID = recipe.ID});
                }
            else //ModelState is not valid
                response = View(recipe);

            return response;
        }

        /// <summary>
        ///     Creates Implementation for RecipeID using default values
        ///     redirects to the Update implementation factory partial view
        /// </summary>
        /// <param name="recipeID"></param>
        /// <returns>redirect to update implementation factory partial</returns>
        [RoleCheck(DefaultRedirect, Admin, PowerUser)]
        public ActionResult CreateImplementationPartial (int recipeID)
        {
            //Return variable
            ActionResult response;
            //ID of implementation created
            var implementationID = default(int);
            try
            {
                implementationID =
                    _ingredientImplementationDAO.CreateIngredientImplementation(
                                                                                new IngredientImplementationPO
                                                                                    {RecipeID = recipeID});
                //Object Initialization was used to allow the DB to use it's default values for an implementation
            }
            catch (Exception e)
            {
                //Error Occurred, redirect with message
                WriteErrorToLog(e);
                TempData["Message"] = e.Data["Problem"];
            }
            finally
            {
                //Both success and fail redirect back to GET
                response = RedirectToAction("EditImplementationPartialFactory", new {implementationID});
            }

            return response;
        }

        /// <summary>
        ///     Partial view that serves as an ajax factory for the edit implementation
        /// </summary>
        /// <param name="implementationID"></param>
        /// <returns></returns>
        [RoleCheck(DefaultRedirect, Admin, PowerUser)]
        public PartialViewResult EditImplementationPartialFactory (int implementationID)
        {
            //Return variable
            ViewBag.Message = TempData["Message"];
            var response = PartialView(implementationID);

            return response;
        }

        /// <summary>
        ///     Partial view that holds the update implementation form
        /// </summary>
        /// <param name="implementationID"></param>
        /// <returns></returns>
        [HttpGet, RoleCheck(DefaultRedirect, Admin, PowerUser)]
        public PartialViewResult EditImplementationPartial (int implementationID)
        {
            //Return variable
            PartialViewResult response;
            try
            {
                //Get implementation data and convert to PO
                var implementationPO =
                    new IngredientImplementationPO(_ingredientImplementationDAO.ReadImplementation(implementationID));
                implementationPO.IngredientDropList = GetIngredients(implementationPO.IngredientID);
                ViewBag.Message = TempData["Message"];

                response = PartialView(implementationPO);
            }
            catch (Exception e)
            {
                //Error occurred, return with message
                WriteErrorToLog(e);
                ViewBag.Message = e.Data["Problem"];
                response = PartialView(new IngredientImplementationPO());
            }

            return response;
        }

        /// <summary>
        ///     Update implementation action that also catches and redirects to delete based on button value
        ///     Redirects back to the Get to enable continuous editing
        /// </summary>
        /// <param name="ingredient">ingredient implementation to update</param>
        /// <param name="button">string value of the clicked button</param>
        /// <returns>Redirects back to GET method</returns>
        /// <exception cref="NotImplementedException">
        ///     Should not be reached, but here in case a button's value
        ///     doesn't match the case value
        /// </exception>
        [HttpPost, RoleCheck(DefaultRedirect, Admin, PowerUser)]
        public ActionResult EditImplementationPartial (IngredientImplementationPO ingredient, string button)
        {
            //Return variable
            ActionResult result;
            switch (button) //Check for update or delete button clicked
            {
                case "Update":
                    try
                    {
                        if (ModelState.IsValid)
                        {
                            //ModelState is good, update and redirect back to GET with success message
                            _ingredientImplementationDAO.UpdateIngredientImplementation(ingredient);
                            TempData["Message"] = "Update Success!";
                            result = RedirectToAction(
                                                      "EditImplementationPartial",
                                                      new {implementationID = ingredient.ImplementationID});
                        }
                        else //ModelState is not valid
                        {
                            //Refresh the dropdown list and return self
                            ingredient.IngredientDropList = GetIngredients(ingredient.IngredientID);
                            result = PartialView(ingredient);
                        }
                    }
                    catch (Exception e)
                    {
                        //Error occurred, return self with message
                        WriteErrorToLog(e);
                        ViewBag.Message = e.Data["Problem"];
                        result = PartialView(ingredient);
                    }

                    break;
                case "Delete":
                    result = RedirectToAction(
                                              "DeleteImplementation",
                                              new {implementationID = ingredient.ImplementationID});
                    break;
                default: //This should not be reached!
                    throw new NotImplementedException();
            }

            return result;
        }

        /// <summary>
        ///     Delete implementation by ID, returns empty partial
        /// </summary>
        /// <param name="implementationID"></param>
        /// <returns>Returns empty html to replace the update partial</returns>
        [RoleCheck(DefaultRedirect, Admin, PowerUser)]
        public ActionResult DeleteImplementation (int implementationID)
        {
            //Return variable
            ActionResult result;
            try
            {
                _ingredientImplementationDAO.DeleteIngredientImplementation(implementationID);
                result = PartialView();
            }
            catch (Exception e)
            {
                //Error occurred, redirect back with message
                WriteErrorToLog(e);
                TempData["Message"] = e.Data["Problem"];
                result = RedirectToAction("EditImplementationPartial", new {implementationID});
            }

            return result;
        }

        /// <summary>
        ///     Gets list of ingredients and builds them into a list of SelectListItems for a dropdown
        /// </summary>
        /// <param name="ingredientID">ingredient that starts selected</param>
        /// <returns></returns>
        private List<SelectListItem> GetIngredients (int ingredientID)
        {
            var result = new List<SelectListItem>();

            try
            {
                //Get list of ingredients and build into List of SelectListItems
                result.AddRange(
                                _ingredientsDAO.ReadAllIngredients()
                                               .Select(
                                                       ingredient => new SelectListItem
                                                       {
                                                           Text = ingredient.Name,
                                                           Value = ingredient.IngredientID.ToString(),
                                                           Selected = ingredient.IngredientID ==
                                                                      ingredientID //Sets the selected item to match ID param
                                                       }));
            }
            catch
            {
                //Error Occurred
                ViewBag.Message = "Problem building dropdown.";
                LogWriter.WriteToLog(messages: "Problem building dropdown.");
            }

            return result;
        }
    }
}