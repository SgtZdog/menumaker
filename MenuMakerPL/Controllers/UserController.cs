﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using MenuMakerBLL;
using MenuMakerDAL;
using MenuMakerPL.Controllers.ActionFilters;
using MenuMakerPL.Models;
using MenuModels;

// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace MenuMakerPL.Controllers
{
    /// <summary>
    ///     User controller for handling login and registration
    /// </summary>
    public class UserController : MMController
    {
        private IngredientImplementationDAO _ingredientImplementationDAO;
        private RecipeDAO _recipeDAO;
        private ShoppingListDAO _shoppingListDAO;
        private UsersDAO _usersDAO;

        public UserController ()
        {
            var connection = ConfigurationManager.ConnectionStrings["MenuMaker"].ConnectionString;
            _usersDAO = new UsersDAO(connection);
            _shoppingListDAO = new ShoppingListDAO(connection);
            _recipeDAO = new RecipeDAO(connection);
            _ingredientImplementationDAO = new IngredientImplementationDAO(connection);
        }

        /// <summary>
        ///     Make the login page the landing page.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index ()
        {
            return RedirectToAction("Login");
        }

        /// <summary>
        ///     Login page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Login ()
        {
            //Return variable
            ActionResult response = View();

            return response;
        }

        /// <summary>
        ///     Login POST, collects login form and logs in user, entire User details are stored in session.
        ///     Subject to change if User grows in size.
        ///     Redirects to Home Page
        /// </summary>
        /// <param name="login">User's login info, child of UserPO</param>
        /// <returns>Redirects to Home Page</returns>
        [HttpPost]
        public ActionResult Login (LoginVM login)
        {
            //Return variable
            ActionResult result;

            try
            {
                if (ModelState.IsValid)
                {
                    var user = new UserPO(_usersDAO.ReadUserByName(login.Username));
                    if (user.Password == login.Password) //Password check
                    {
                        user.Password = null; //Scrub password
                        Session["User"] = user; //Set session
                        result = RedirectToAction("Index", "Home");
                    }
                    else //password mismatch
                    {
                        ViewBag.Message = "Login is incorrect!";
                        result = View(login);
                    }
                }
                else //ModelState not valid
                {
                    result = View(login);
                }
            }
            catch (Exception e)
            {
                //Error occurred, return to user with message.
                WriteErrorToLog(e);
                ViewBag.Message = e.Data["Problem"];
                result = View(login);
            }

            return result;
        }

        /// <summary>
        ///     Logout button to remove session and redirect to home page
        /// </summary>
        /// <returns>Redirect to home page</returns>
        public ActionResult Logout ()
        {
            Session["User"] = null;
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        ///     Registration page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Register ()
        {
            //Return variable
            ActionResult result = View();

            return result;
        }

        /// <summary>
        ///     Registration form collection. Default role of StandardUser set here (role id == 1).
        ///     Also checks against the VerificationCode set in the RegisterVM
        /// </summary>
        /// <param name="user">User to register, child of LoginVM (which is child of UserPO)</param>
        /// <returns>Redirects to login page so user can test successful login</returns>
        [HttpPost]
        public ActionResult Register (RegisterVM user)
        {
            //Return variable
            ActionResult result;

            if (ModelState.IsValid && user.Verification == user.VerificationCode)
                try
                {
                    user.RoleID = 1; //Sets role to StandardUser 
                    _usersDAO.CreateUser(user);
                    result = RedirectToAction("Login");
                }
                catch (Exception e)
                {
                    //Error occurred, return  with message
                    WriteErrorToLog(e);
                    ViewBag.Message = e.Data["Problem"];
                    result = View(user);
                }
            else //ModelState not valid or Verification mismatch
                result = View(user);

            return result;
        }

        /// <summary>
        ///     Admin level user search
        /// </summary>
        /// <returns></returns>
        [HttpGet, RoleCheck(DefaultRedirect, Admin)]
        public ActionResult SearchUser ()
        {
            ViewBag.Message = TempData["Message"];
            //Return variable
            ActionResult result = View();

            return result;
        }

        /// <summary>
        ///     Admin user search for getting a user to update. Redirects to Update/Delete page
        /// </summary>
        /// <param name="user">User to search for</param>
        /// <returns>Redirect to Update/Delete page</returns>
        [HttpPost, RoleCheck(DefaultRedirect, Admin)]
        public ActionResult SearchUser (UserPO user)
        {
            //Return variable
            ActionResult result;
            if (ModelState.IsValid)
                try
                {
                    result = RedirectToAction("UpdateDeleteUser", new {username = user.Username});
                }
                catch (Exception e)
                {
                    //Error occurred, return with message
                    WriteErrorToLog(e);
                    ViewBag.Message = e.Data["Problem"];
                    result = View(user);
                }
            else //ModelState is not valid
                result = View(user);

            return result;
        }

        /// <summary>
        ///     Admin page for updating or deleting a user
        /// </summary>
        /// <param name="username">username of user to edit</param>
        /// <returns></returns>
        [HttpGet, RoleCheck(DefaultRedirect, Admin)]
        public ActionResult UpdateDeleteUser (string username)
        {
            //Return Variable
            ActionResult result;
            ViewBag.Message = TempData["Message"];
            try
            {
                var userPO = new UserPO(_usersDAO.ReadUserByName(username));
                result = View(userPO);
            }
            catch (Exception e)
            {
                //Error occurred, redirect back to search with message
                WriteErrorToLog(e);
                TempData["Message"] = "Error finding user " + username + ".";
                result = RedirectToAction("SearchUser");
            }

            return result;
        }

        /// <summary>
        ///     Update user form collection.
        /// </summary>
        /// <param name="user">user info to update</param>
        /// <returns>Redirects to the update/delete user page for continuous editing</returns>
        [HttpPost, RoleCheck(DefaultRedirect, Admin)]
        public ActionResult UpdateDeleteUser (UserPO user)
        {
            //Return Variable
            ActionResult result;
            if (ModelState.IsValid)
                try
                {
                    _usersDAO.UpdateUser(user);
                    //Redirect back when done
                    result = RedirectToAction("UpdateDeleteUser", new {username = user.Username});
                }
                catch (Exception e)
                {
                    //Error occurred, return with message.
                    WriteErrorToLog(e);
                    ViewBag.Message = e.Data["Problem"];
                    result = View(user);
                }
            else //ModelState is not valid
                result = View(user);

            return result;
        }

        /// <summary>
        ///     Delete user button collection
        /// </summary>
        /// <param name="userID">ID of user to delete</param>
        /// <returns></returns>
        [RoleCheck(DefaultRedirect, Admin)]
        public ActionResult DeleteUser (int userID)
        {
            ActionResult result;
            try
            {
                _usersDAO.DeleteUser(userID);
                //Redirect back to search when done
                result = RedirectToAction("SearchUser");
            }
            catch (Exception e)
            {
                //Error occurred, return with message
                WriteErrorToLog(e);
                TempData["Message"] = e.Data["Problem"];
                try
                {
                    //Get back the username and attempt to redirect back to update
                    var username = new UserPO(_usersDAO.ReadUser(userID)).Username;
                    result = RedirectToAction("UpdateDeleteUser", new {username});
                }
                catch (Exception e2)
                {
                    //Error occurred while trying to find user to do redirect, redirect to search with new message
                    Console.WriteLine(e2);
                    TempData["Message"] = e2.Data["Problem"] as string + "Occurred while trying to delete.";
                    result = RedirectToAction("SearchUser");
                }
            }

            return result;
        }

        /// <summary>
        ///     Shopping List page for the user
        /// </summary>
        /// <returns>ShoppingList view</returns>
        [HttpGet, RoleCheck(DefaultRedirect, StandardUser, PowerUser, Admin)]
        public ActionResult ShoppingList ()
        {
            ViewBag.Message = TempData["Message"];
            //Return variable
            ActionResult result;

            try
            {
                var recipeIDs = _shoppingListDAO.ReadAllShoppingListRecipes(((UserPO)Session["User"]).ID);
                var recipes = recipeIDs.Select(
                                               recipeID =>
                                                   new RecipePO(_recipeDAO.ReadRecipe(recipeID)))
                                       .ToList();
                var ingredients = new List<IngredientImplementation>();
                foreach (var recipe in recipes)
                    ingredients.AddRange(_ingredientImplementationDAO.ReadRecipeIngredients(recipe.ID));
                var consolidatedIngredients = IngredientImplementationBLO.ConsolidateIngredients(ingredients).ToList();

                var shoppingList = new FullShoppingListVM(
                                                          recipes,
                                                          consolidatedIngredients,
                                                          IngredientImplementationBLO.SumIngredientsCost(
                                                                                                         consolidatedIngredients));
                Session["ShoppingList"] = shoppingList;
                result = View();
            }
            catch (Exception e)
            {
                WriteErrorToLog(e);
                TempData["Message"] = e.Data["Problem"];
                result = RedirectToAction("Index", "Home");
            }

            return result;
        }

        /// <summary>
        ///     Deletes a Recipe from the User's shopping list
        /// </summary>
        /// <param name="recipeID"></param>
        /// <returns>Redirects to ShoppingList page</returns>
        [RoleCheck(DefaultRedirect, StandardUser, PowerUser, Admin)]
        public ActionResult DeleteShoppingListRecipe (int recipeID)
        {
            try
            {
                _shoppingListDAO.DeleteShoppingListRecipe(((UserPO)Session["User"]).ID, recipeID);
            }
            catch (Exception e)
            {
                //Error occurred, return with message
                WriteErrorToLog(e);
                TempData["Message"] = e.Data["Problem"];
            }

            return RedirectToAction("ShoppingList");
        }

        /// <summary>
        ///     Clears a user's shopping list
        /// </summary>
        /// <returns>Redirects to ShoppingList page</returns>
        [RoleCheck(DefaultRedirect, StandardUser, PowerUser, Admin)]
        public ActionResult DeleteAllShoppingListRecipes ()
        {
            try
            {
                _shoppingListDAO.DeleteAllShoppingListRecipes(((UserPO)Session["User"]).ID);
            }
            catch (Exception e)
            {
                //Error occurred, return with message
                WriteErrorToLog(e);
                TempData["Message"] = e.Data["Problem"];
            }

            return RedirectToAction("ShoppingList");
        }

        /// <summary>
        ///     Adds a recipe to the users shopping list, no return, should be called with Ajax or other no-action
        /// </summary>
        /// <param name="recipeID"></param>
        [RoleCheck(DefaultRedirect, StandardUser, PowerUser, Admin)]
        public void AddRecipeToShoppingList (int recipeID)
        {
            try
            {
                _shoppingListDAO.CreateShoppingListRecipe(((UserPO)Session["User"]).ID, recipeID);
            }
            catch (Exception e)
            {
                //Error occurred, return with message
                WriteErrorToLog(e);
                TempData["Message"] = e.Data["Problem"];
            }
        }
    }
}