﻿using System;
using System.Web.Mvc;
using Logger;

namespace MenuMakerPL.Controllers
{
    using static LogWriter;

    // ReSharper disable once InconsistentNaming
    /// <summary>
    ///     Parent controller for this project
    /// </summary>
    public abstract class MMController : Controller
    {
        //Values to be used for RoleCheck to avoid string literals
        protected const string DefaultRedirect = "~/Home/Index";
        protected const string Admin = "Admin";
        protected const string PowerUser = "PowerUser";
        protected const string StandardUser = "StandardUser";

        //basic data access to use if controller only needs one.
        //Should be overridden with new to specify DAO type if used.
        //Should not be used if controller needs more than one!

        /// <summary>
        ///     Writes error to log if not already logged
        ///     Checks against .Data["IsLogged"]
        /// </summary>
        /// <param name="e"></param>
        protected static void WriteErrorToLog (Exception e)
        {
            if (!(bool)e.Data["IsLogged"]) WriteToLog(e);
        }
    }
}