﻿using System.Web.Mvc;
using MenuMakerPL.Controllers.ActionFilters;
using MenuMakerPL.Models;

namespace MenuMakerPL.Controllers
{
    /// <summary>
    ///     Landing page controller with basic site pages
    /// </summary>
    public class HomeController : MMController
    {
        /// <summary>
        ///     Home page, redirects to SearchUser if logged in as admin
        /// </summary>
        /// <returns>Home page or SearchUser if admin</returns>
        public ActionResult Index ()
        {
            ActionResult result = View();
            if (Admin == ((UserPO)Session["User"])?.RoleName) result = RedirectToAction("SearchUser", "User");

            return result;
        }

        /// <summary>
        ///     Contact page, can only be viewed if a registered user.
        /// </summary>
        /// <returns></returns>
        [RoleCheck(DefaultRedirect, StandardUser, PowerUser, Admin)]
        public ActionResult Contact ()
        {
            return View();
        }
    }
}