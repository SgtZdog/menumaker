﻿using System.Linq;
using System.Web.Mvc;
using MenuMakerPL.Models;

namespace MenuMakerPL.Controllers.ActionFilters
{
    /// <summary>
    ///     Action Filter Attribute to check if Required role is met
    /// </summary>
    public class RoleCheck : ActionFilterAttribute
    {
        private readonly string _redirect;
        private readonly string[] _targetRoles;

        /// <summary>
        ///     Take in the redirect route and valid roles
        ///     WILL CAUSE UNEXPECTED BEHAVIOR IF REDIRECT IS FORGOTTEN
        /// </summary>
        /// <param name="redirect"></param>
        /// <param name="targetRoles"></param>
        public RoleCheck (string redirect, params string[] targetRoles)
        {
            _redirect = redirect;
            _targetRoles = targetRoles;
        }

        /// <summary>
        ///     Compare current RoleName to valid role names and redirect if no match
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting (ActionExecutingContext filterContext)
        {
            var role = ((UserPO)filterContext.HttpContext.Session["User"])?.RoleName;
            if (role == null || !_targetRoles.Contains(role))
                //No role found or role does not match
                filterContext.Result = new RedirectResult(_redirect);

            base.OnActionExecuting(filterContext);
        }
    }
}