/**
 * @return {boolean}
 */
function ConfirmDelete(Name) {
    return confirm('Are you sure you want to Delete ' + Name + '?');
}