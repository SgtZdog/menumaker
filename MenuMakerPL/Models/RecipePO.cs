﻿using System.ComponentModel.DataAnnotations;
using MenuMakerPL.Models.MetaData;
using MenuModels;

namespace MenuMakerPL.Models
{
    /// <summary>
    ///     Recipe PO version
    ///     Data Annotations in the MetaData
    /// </summary>
    [MetadataType(typeof(RecipeMetaData))]
    public class RecipePO : Recipe
    {
        /// <summary>
        ///     Used for Views
        /// </summary>
        public RecipePO () { }

        /// <summary>
        ///     Used for mapping
        /// </summary>
        /// <param name="recipe"></param>
        public RecipePO (Recipe recipe)
            : base(recipe) { }
    }
}