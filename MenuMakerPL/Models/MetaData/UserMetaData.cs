﻿// ReSharper disable UnusedMember.Global

using System.ComponentModel.DataAnnotations;

namespace MenuMakerPL.Models.MetaData
{
    public class UserMetaData
    {
        public object ID { get; set; }

        public object RoleID { get; set; }

        [MaxLength(20, ErrorMessage = "Username cannot be more than 20 characters.")]
        public virtual object Username { get; set; }

        [MaxLength(50, ErrorMessage = "Password cannot be more than 50 characters.")]
        public virtual object Password { get; set; }

        public string RoleName { get; set; }

        public string RoleDescription { get; set; }
    }
}