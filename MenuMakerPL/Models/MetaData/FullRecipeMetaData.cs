﻿using System.ComponentModel.DataAnnotations;

namespace MenuMakerPL.Models.MetaData
{
    public class FullRecipeMetaData
    {
        [DataType(DataType.Currency, ErrorMessage = "Must be a number."), DisplayFormat(DataFormatString = "{0:c}")]
        public object Cost { get; set; }
    }
}