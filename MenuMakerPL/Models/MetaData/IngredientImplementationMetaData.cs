﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MenuMakerPL.Models.MetaData
{
    public class IngredientImplementationMetaData : IngredientMetaData
    {
        [HiddenInput(DisplayValue = false)] public object RecipeID { get; set; }

        [HiddenInput(DisplayValue = false), Editable(false)]
        public object ImplementationID { get; set; }

        public object Quantity { get; set; }

        [DataType(DataType.Currency, ErrorMessage = "Must be a number."),
         DisplayFormat(DataFormatString = "{0:c}", ApplyFormatInEditMode = true)]
        public object Cost { get; set; }
    }
}