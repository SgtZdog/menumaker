﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MenuMakerPL.Models.MetaData
{
    public class RecipeMetaData
    {
        [HiddenInput(DisplayValue = false)] public object ID { get; set; }

        [MaxLength(50, ErrorMessage = "Name cannot be more than 50 characters."),
         Required(ErrorMessage = "Name is required.")]
        public object Name { get; set; }

        public object Directions { get; set; }

        public object Description { get; set; }
    }
}