﻿using System.ComponentModel.DataAnnotations;

namespace MenuMakerPL.Models.MetaData
{
    public class LoginMetaData : UserMetaData
    {
        [Required(ErrorMessage = "Username required."), MaxLength(20, ErrorMessage = "Invalid Username.")]
        public override object Username { get; set; }

        [Required(ErrorMessage = "Password required."), MaxLength(50, ErrorMessage = "Password Invalid.")]
        public override object Password { get; set; }
    }
}