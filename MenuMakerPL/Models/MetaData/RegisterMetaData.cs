﻿using System.ComponentModel.DataAnnotations;

namespace MenuMakerPL.Models.MetaData
{
    // ReSharper disable once ClassWithVirtualMembersNeverInherited.Global
    public class RegisterMetaData : LoginMetaData
    {
        [Required(ErrorMessage = "Password confirmation required."), MaxLength(50, ErrorMessage = "Password Invalid."),
         Compare(nameof(Password), ErrorMessage = "Passwords must match!")]
        public virtual object PasswordConfirmation { get; set; }

        [Required(ErrorMessage = "Verification code required."),
         Compare(nameof(VerificationCode), ErrorMessage = "Incorrect verification.")]
        public virtual object Verification { get; set; }

        // ReSharper disable once MemberCanBeProtected.Global
        // ReSharper disable once UnassignedGetOnlyAutoProperty
        // ReSharper disable once UnusedAutoPropertyAccessor.Global
        public virtual object VerificationCode { get; }
    }
}