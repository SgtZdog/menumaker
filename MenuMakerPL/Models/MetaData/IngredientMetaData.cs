﻿// ReSharper disable UnusedMember.Global

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MenuMakerPL.Models.MetaData
{
    public class IngredientMetaData
    {
        [HiddenInput(DisplayValue = false)] public object IngredientID { get; set; }

        [MaxLength(50, ErrorMessage = "Name cannot be more than 50 characters."),
         Required(ErrorMessage = "Name is required.")]
        public object Name { get; set; }

        [MaxLength(50, ErrorMessage = "Unit cannot be more than 50 characters.")]
        public object Unit { get; set; }

        [DisplayName("Unit Price"), DataType(DataType.Currency, ErrorMessage = "Must be a number."),
         DisplayFormat(DataFormatString = "{0:c}", ApplyFormatInEditMode = true)]
        public double Price { get; set; }
    }
}