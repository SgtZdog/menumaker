﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MenuMakerPL.Models.MetaData;
using MenuModels;

namespace MenuMakerPL.Models
{
    /// <summary>
    ///     This contains the basic details and ingredients for a recipe as well as optionally the total cost
    ///     Data Annotations in MetaData
    /// </summary>
    [MetadataType(typeof(FullRecipeMetaData))]
    public class FullRecipeVM
    {
        /// <summary>
        ///     Used for Views
        /// </summary>
        public FullRecipeVM () { }

        /// <summary>
        ///     Used for Views and forms, cost is optional since it is only used by the recipe details page
        ///     Cost constructor is deprecated with plans to change to custom Get Accessor in property with no set
        ///     Converts objects to PO versions automatically
        /// </summary>
        /// <param name="recipe"></param>
        /// <param name="ingredients"></param>
        /// <param name="cost"></param>
        public FullRecipeVM (
            Recipe recipe,
            IEnumerable<IngredientImplementation> ingredients,
            double cost = default(double))
        {
            Recipe = new RecipePO(recipe);
            //Convert ingredients to PO versions
            foreach (var ingredient in ingredients) Ingredients.Add(new IngredientImplementationPO(ingredient));

            Cost = cost;
        }

        public RecipePO Recipe { get; set; }
        public List<IngredientImplementationPO> Ingredients { get; set; } = new List<IngredientImplementationPO>();
        public double Cost { get; set; }
    }
}