﻿using System.ComponentModel.DataAnnotations;
using MenuMakerPL.Models.MetaData;

namespace MenuMakerPL.Models
{
    /// <inheritdoc />
    /// <summary>
    ///     Login VM for using Login MetaData on User objects
    /// </summary>
    [MetadataType(typeof(LoginMetaData))]
    public class LoginVM : UserPO { }
}