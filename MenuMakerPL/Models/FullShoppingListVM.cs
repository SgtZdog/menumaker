using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MenuMakerPL.Models.MetaData;
using MenuModels;

namespace MenuMakerPL.Models
{
    /// <summary>
    ///     This contains all of the ingredients in a shopping list and the total
    ///     Data Annotations in MetaData
    /// </summary>
    [MetadataType(typeof(FullShoppingListMetaData))]
    public class FullShoppingListVM
    {
        /// <summary>
        ///     Used for Views
        /// </summary>
        public FullShoppingListVM () { }

        /// <summary>
        ///     Used for Views and forms
        ///     Converts objects to PO versions automatically
        /// </summary>
        /// <param name="recipes"></param>
        /// <param name="ingredients"></param>
        /// <param name="cost"></param>
        public FullShoppingListVM (
            IEnumerable<Recipe> recipes,
            IEnumerable<IngredientImplementation> ingredients,
            double cost)
        {
            //convert list types 
            foreach (var Recipe in recipes) Recipes.Add(new RecipePO(Recipe));
            foreach (var Ingredient in ingredients) Ingredients.Add(new IngredientImplementationPO(Ingredient));
            Cost = cost;
        }

        public List<RecipePO> Recipes { get; set; } = new List<RecipePO>();
        public List<IngredientImplementationPO> Ingredients { get; set; } = new List<IngredientImplementationPO>();
        public double Cost { get; set; }
    }
}