﻿using System.ComponentModel.DataAnnotations;
using MenuMakerPL.Models.MetaData;
using MenuModels;

namespace MenuMakerPL.Models
{
    /// <summary>
    ///     User PO version
    ///     Data Annotations in MetaData
    /// </summary>
    [MetadataType(typeof(UserMetaData))]
    public class UserPO : User
    {
        // ReSharper disable once MemberCanBeProtected.Global
        /// <summary>
        /// </summary>
        public UserPO () { }

        public UserPO (User user)
            : base(user) { }
    }
}