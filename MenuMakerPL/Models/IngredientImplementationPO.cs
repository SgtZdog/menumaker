﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MenuMakerPL.Models.MetaData;
using MenuModels;

namespace MenuMakerPL.Models
{
    /// <summary>
    ///     Ingredient Implementation PO version
    ///     Data Annotations in the MetaData
    /// </summary>
    [MetadataType(typeof(IngredientImplementationMetaData))]
    public class IngredientImplementationPO : IngredientImplementation
    {
        /// <summary>
        ///     Used for views
        /// </summary>
        public IngredientImplementationPO () { }

        /// <summary>
        ///     Used for mapping
        /// </summary>
        /// <param name="implementation"></param>
        public IngredientImplementationPO (IngredientImplementation implementation)
            : base(implementation) { }

        public List<SelectListItem> IngredientDropList { get; set; }
    }
}