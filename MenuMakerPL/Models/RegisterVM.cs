﻿using System.ComponentModel.DataAnnotations;
using MenuMakerPL.Models.MetaData;

namespace MenuMakerPL.Models
{
    /// <inheritdoc />
    /// <summary>
    ///     Registration VM for using Registration MetaData on User/Login objects
    /// </summary>
    [MetadataType(typeof(RegisterMetaData))]
    public class RegisterVM : LoginVM
    {
        public string PasswordConfirmation { get; set; }
        public string VerificationCode { get; } = "8/30";
        public string Verification { get; set; }
    }
}