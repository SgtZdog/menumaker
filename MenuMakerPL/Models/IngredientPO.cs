﻿using System.ComponentModel.DataAnnotations;
using MenuMakerPL.Models.MetaData;
using MenuModels;

namespace MenuMakerPL.Models
{
    /// <summary>
    ///     Ingredient PO version
    ///     Data Annotations in the MetaData
    /// </summary>
    [MetadataType(typeof(IngredientMetaData))]
    public class IngredientPO : Ingredient
    {
        /// <summary>
        ///     Used for Views
        /// </summary>
        public IngredientPO () { }

        /// <summary>
        ///     Used for mapping
        /// </summary>
        /// <param name="ingredient"></param>
        public IngredientPO (Ingredient ingredient)
            : base(ingredient) { }
    }
}