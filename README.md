This was created as a capstone project in the bootcamp for Onshore Outsourcing. I achieved top 3 in my class and was hired on. 

Not everything in this solution is ideal. I was building specifically to match up to a grading rubric. 
This includes using 3 distinct projects (one for each layer), even though I do very little in my business layer. My GUI is very lean and not much to look at it. This is because I specifically didn't want to be put on a front end development team. 

The .bak file is a SQL database that this uses as the data backend. You will also need the Logger project also available on my bitbucket. The Logger exists because I had to have a logger, I wanted mine to be a distinct reuseable piece, and I couldn't use one of the many loggers out there. 