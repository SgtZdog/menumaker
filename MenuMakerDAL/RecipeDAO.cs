﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MenuMakerDAL.Models;
using MenuModels;

namespace MenuMakerDAL
{
    /// <summary>
    ///     Recipe Data Access object. No default ctor to force setting the connection string.
    /// </summary>
    public class RecipeDAO : DAO
    {
        public RecipeDAO (string connection)
            : base(connection) { }

        /// <summary>
        ///     Creates recipe with basic info. Does not create with any initialized ingredients.
        /// </summary>
        /// <param name="recipeToCreate">Recipe to create</param>
        /// <returns>Returns recipe ID of newly created recipe</returns>
        public int CreateRecipe (Recipe recipeToCreate)
        {
            //Convert to DO for any conversion logic.
            var recipeDOToCreate = new RecipeDO(recipeToCreate);
            //Return variable
            var createdRecipeID = default(int);
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var command = new SqlCommand("CREATE_RECIPE", connection))
                    {
                        //Setup stored procedure.
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Name", recipeDOToCreate.Name);
                        command.Parameters.AddWithValue("@Directions", recipeDOToCreate.Directions);
                        command.Parameters.AddWithValue("@Description", recipeDOToCreate.Description);

                        //Open connection and execute, collect returned recipe ID
                        connection.Open();
                        createdRecipeID = Convert.ToInt32(command.ExecuteScalar());
                    }
                }
            }
            catch (Exception e)
            {
                e.Data["Problem"] = "Problem creating recipe.";
                WriteErrorToLog(e);
            }

            return createdRecipeID;
        }

        /// <summary>
        ///     Gets list of all recipes in db with basic info, no ingredients
        /// </summary>
        /// <returns>List of Recipes</returns>
        public List<Recipe> ReadAllRecipes ()
        {
            //Return variable
            var recipes = new List<Recipe>();

            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var command = new SqlCommand("READ_ALL_RECIPES", connection))
                    {
                        //Setup stored procedure
                        command.CommandType = CommandType.StoredProcedure;

                        //Open connection and execute, collect recipe list
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            //Add each recipe to the list
                            while (reader.Read())
                            {
                                recipes.Add(
                                            new RecipeDO(
                                                         reader["Name"] as string,
                                                         reader["Directions"] as string,
                                                         reader["Description"] as string,
                                                         (int)reader["ID"]));
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.Data["Problem"] = "Problem reading recipe list.";
                WriteErrorToLog(e);
            }

            return recipes;
        }

        /// <summary>
        ///     Gets single recipe by id with basic details, does not get ingredients
        /// </summary>
        /// <param name="recipeID">Id of recipe</param>
        /// <returns>Recipe requested</returns>
        public Recipe ReadRecipe (int recipeID)
        {
            //Return variable
            RecipeDO recipeDO = null;

            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var command = new SqlCommand("READ_RECIPE", connection))
                    {
                        //Setup stored proedure
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", recipeID);

                        //Open connection and execute, collect returned recipe
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                                recipeDO = new RecipeDO(
                                                        reader["Name"] as string,
                                                        reader["Directions"] as string,
                                                        reader["Description"] as string,
                                                        (int)reader["ID"]);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.Data["Problem"] = "Problem reading recipe.";
                WriteErrorToLog(e);
            }

            return recipeDO;
        }

        /// <summary>
        ///     Update  recipe basic info, will not update ingredients.
        /// </summary>
        /// <param name="recipe">Recipe to update</param>
        public void UpdateRecipe (Recipe recipe)
        {
            //Convert to DO for conversion logic
            var recipeDO = new RecipeDO(recipe);
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var command = new SqlCommand("UPDATE_RECIPE", connection))
                    {
                        //Setup stored procedure
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", recipeDO.ID);
                        command.Parameters.AddWithValue("@Name", recipeDO.Name);
                        command.Parameters.AddWithValue("@Directions", recipeDO.Directions);
                        command.Parameters.AddWithValue("@Description", recipeDO.Description);

                        //Open connnection and execute, no return to collect.
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                e.Data["Problem"] = "Problem updating recipe.";
                WriteErrorToLog(e);
            }
        }

        /// <summary>
        ///     Delete recipe by ID, DB should cascade the ingredient implementations deletion
        /// </summary>
        /// <param name="recipeID">Id of recipe to delete</param>
        public void DeleteRecipe (int recipeID)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var command = new SqlCommand("DELETE_RECIPE", connection))
                    {
                        //Setup stored procedure
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", recipeID);

                        //Open connection and execute, no return to collect
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                e.Data["Problem"] = "Problem deleting recipe.";
                WriteErrorToLog(e);
            }
        }
    }
}