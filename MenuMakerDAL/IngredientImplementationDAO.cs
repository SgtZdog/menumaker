﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MenuMakerDAL.Models;
using MenuModels;

namespace MenuMakerDAL
{
    /// <summary>
    ///     Ingredient Implementation Data Access. No default ctor to force setting the connection string.
    /// </summary>
    public class IngredientImplementationDAO : DAO
    {
        public IngredientImplementationDAO (string connection)
            : base(connection) { }

        /// <summary>
        ///     Creates Ingredient Implementation in link table.
        ///     Does NOT create an ingredient, ingredient must already exist.
        ///     Returns the ImplementationID for the created row.
        /// </summary>
        /// <param name="implementationToCreate">Ingredient Implementation that will be created.</param>
        /// <returns>Implementation ID for the new object.</returns>
        public int CreateIngredientImplementation (IngredientImplementation implementationToCreate)
        {
            //Convert to DO to allow for any needed conversion logic.
            var implementationDOToCreate =
                new IngredientImplementationDO(implementationToCreate);
            //Return variable.
            var newImplementationID = default(int);
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var command = new SqlCommand("CREATE_INGREDIENT_IMPLEMENTATION", connection))
                    {
                        //Establish Stored Procedure values.
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@RecipeID", implementationDOToCreate.RecipeID);
                        command.Parameters.AddWithValue("@IngredientID", implementationDOToCreate.IngredientID);
                        command.Parameters.AddWithValue("@Quantity", implementationDOToCreate.Quantity);

                        //Open connection and collect return value.
                        connection.Open();
                        newImplementationID = Convert.ToInt32(command.ExecuteScalar());
                    }
                }
            }
            catch (Exception e)
            {
                e.Data["Problem"] = "Problem creating implementation.";
                WriteErrorToLog(e);
            }

            return newImplementationID;
        }

        /// <summary>
        ///     Gathers list of Ingredient Implementations for a given Recipe ID with all of the ingredient details.
        /// </summary>
        /// <param name="recipeID">Recipe ID to search by.</param>
        /// <returns>List of Ingredient Implementations</returns>
        public List<IngredientImplementation> ReadRecipeIngredients (int recipeID)
        {
            //Return variable
            var ingredients = new List<IngredientImplementation>();

            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var command = new SqlCommand("READ_RECIPE_INGREDIENTS", connection))
                    {
                        //Setup Stored Procedure.
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@RecipeID", recipeID);

                        //Open Connection
                        connection.Open();

                        //Add each Ingredient Implementation to the List.
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ingredients.Add(
                                                new IngredientImplementationDO(
                                                                               (int)reader["RecipeID"],
                                                                               (double)reader["Quantity"],
                                                                               new IngredientDO(
                                                                                                reader["IngredientName"]
                                                                                                    as string,
                                                                                                reader["Unit"] as
                                                                                                    string,
                                                                                                (double)reader["Price"],
                                                                                                (int)reader
                                                                                                    ["IngredientID"]),
                                                                               (int)reader["ImplementationID"]));
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.Data["Problem"] = "Problem reading recipe ingredients.";
                WriteErrorToLog(e);
            }

            return ingredients;
        }

        /// <summary>
        ///     Gets entire Ingredient Implementation with ingredient details based on Implementation ID.
        /// </summary>
        /// <param name="implementationID">ID of Implementation to retrieve.</param>
        /// <returns>Ingredient Implementation to receive.</returns>
        public IngredientImplementation ReadImplementation (int implementationID)
        {
            //Return variable.
            IngredientImplementationDO ingredient = null;

            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var command = new SqlCommand("READ_INGREDIENT_IMPLEMENTATION", connection))
                    {
                        //Setup stored procedure.
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", implementationID);

                        //Open Connection.
                        connection.Open();

                        //Collect return value.
                        using (var reader = command.ExecuteReader())
                        {
                            //No while loop used since single ingredient.
                            if (reader.Read())
                                ingredient =
                                    new IngredientImplementationDO(
                                                                   (int)reader["RecipeID"],
                                                                   (double)reader["Quantity"],
                                                                   new IngredientDO(
                                                                                    reader["IngredientName"]
                                                                                        as string,
                                                                                    reader["Unit"] as string,
                                                                                    (double)reader["Price"],
                                                                                    (int)reader["IngredientID"]),
                                                                   (int)reader["ImplementationID"]);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.Data["Problem"] = "Problem reading implementation.";
                WriteErrorToLog(e);
            }

            return ingredient;
        }

        /// <summary>
        ///     Updates Ingredient Implementation, does NOT update ingredient details.
        ///     Can update all values except the Primary Key, implementation ID
        /// </summary>
        /// <param name="implementation">Implementation to update</param>
        public void UpdateIngredientImplementation (IngredientImplementation implementation)
        {
            var ingredient = new IngredientImplementationDO(implementation);
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var command = new SqlCommand("UPDATE_INGREDIENT_IMPLEMENTATION", connection))
                    {
                        //Setup Stored procedure
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", ingredient.ImplementationID);
                        command.Parameters.AddWithValue("@RecipeID", ingredient.RecipeID);
                        command.Parameters.AddWithValue("@IngredientID", ingredient.IngredientID);
                        command.Parameters.AddWithValue("@Quantity", ingredient.Quantity);

                        //Open connection and execute, no return to collect.
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                e.Data["Problem"] = "Problem updating implementation.";
                WriteErrorToLog(e);
            }
        }

        /// <summary>
        ///     Deletes implementation by implementation id.
        /// </summary>
        /// <param name="implementationID">id of implementation to delete.</param>
        public void DeleteIngredientImplementation (int implementationID)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var command = new SqlCommand("DELETE_INGREDIENT_IMPLEMENTATION", connection))
                    {
                        //Setup stored procedure
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", implementationID);

                        //Open connection and execute, no return to collect.
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                e.Data["Problem"] = "Problem deleting implementation.";
                WriteErrorToLog(e);
            }
        }
    }
}