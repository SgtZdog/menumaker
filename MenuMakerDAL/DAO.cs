﻿using System;
using Logger;

namespace MenuMakerDAL
{
    using static LogWriter;

    /// <summary>
    ///     Parent class for Data Access objects. Default ctor should only be used during development on this solution.
    /// </summary>
    public abstract class DAO
    {
        protected string ConnectionString;

        /// <summary>
        ///     Preferred ctor, should be used for live implementation.
        /// </summary>
        /// <param name="connection">SQL connection string.</param>
        protected DAO (string connection)
        {
            ConnectionString = connection;
        }

        /// <summary>
        ///     To be used if the connection string needs updating.
        /// </summary>
        /// <param name="connection">SQL connection string.</param>
        public virtual void ReplaceConnection (string connection)
        {
            ConnectionString = connection;
        }

        /// <summary>
        ///     Error logger. Passes exception to Logger and marks error as logged in .Data["IsLogged"] = true
        ///     Will throw if it fails, throws the original back in the finally to allow other layers to see the exception.
        /// </summary>
        /// <param name="e">System.Exception to take in</param>
        /// <exception cref="Exception"></exception>
        protected static void WriteErrorToLog (Exception e)
        {
            try
            {
                WriteToLog(e);
                e.Data["IsLogged"] = true;
            }
            finally
            {
                //Throw for other layers to see.
                throw e;
            }
        }
    }
}