using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace MenuMakerDAL
{
    /// <summary>
    ///     Data Access for Shopping List Table
    /// </summary>
    public class ShoppingListDAO : DAO
    {
        /// <summary>
        ///     Basic ctor
        /// </summary>
        /// <param name="connection"></param>
        public ShoppingListDAO (string connection)
            : base(connection) { }

        /// <summary>
        ///     Adds a recipe to the user's shopping list
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="recipeID"></param>
        public void CreateShoppingListRecipe (int userID, int recipeID)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var command = new SqlCommand("CREATE_SHOPPING_LIST_RECIPE", connection))
                    {
                        //Establish Stored Procedure values
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@UserID", userID);
                        command.Parameters.AddWithValue("@RecipeID", recipeID);

                        //Open connection and execute, no return to collect.
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                e.Data["Problem"] = "Problem adding Recipe to Shopping List.";
                WriteErrorToLog(e);
            }
        }

        /// <summary>
        ///     Gets the list of recipes on the user's shopping list
        /// </summary>
        /// <param name="userID"></param>
        /// <returns>List of recipeIDs</returns>
        public IEnumerable<int> ReadAllShoppingListRecipes (int userID)
        {
            var recipes = new List<int>();

            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var command = new SqlCommand("READ_ALL_SHOPPING_LIST_RECIPES", connection))
                    {
                        //Establish Stored Procedure values
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@UserID", userID);

                        //Open connection and collect data
                        connection.Open();
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read()) recipes.Add((int)dataReader["RecipeID"]);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.Data["Problem"] = "Problem reading recipes from shopping list.";
                WriteErrorToLog(e);
            }

            return recipes;
        }

        /// <summary>
        ///     Deletes an individual recipe from a user's shopping list
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="recipeID"></param>
        public void DeleteShoppingListRecipe (int userID, int recipeID)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var command = new SqlCommand("DELETE_SHOPPING_LIST_RECIPE", connection))
                    {
                        //Establish Stored Procedure values
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@UserID", userID);
                        command.Parameters.AddWithValue("@RecipeID", recipeID);

                        //Open connection and execute, no return to collect.
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                e.Data["Problem"] = "Problem removing Recipe from Shopping List.";
                WriteErrorToLog(e);
            }
        }

        /// <summary>
        ///     Clears user's entire shopping list
        /// </summary>
        /// <param name="userID"></param>
        public void DeleteAllShoppingListRecipes (int userID)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var command = new SqlCommand("DELETE_ALL_SHOPPING_LIST_RECIPES", connection))
                    {
                        //Establish Stored Procedure values
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@UserID", userID);

                        //Open connection and execute, no return to collect.
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                e.Data["Problem"] = "Problem removing Recipes from Shopping List.";
                WriteErrorToLog(e);
            }
        }
    }
}