﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MenuMakerDAL.Models;
using MenuModels;

namespace MenuMakerDAL
{
    /// <summary>
    ///     Ingredients Data Access object. No default ctor to force setting connection string.
    /// </summary>
    public class IngredientsDAO : DAO
    {
        public IngredientsDAO (string connection)
            : base(connection) { }

        /// <summary>
        ///     Creates an Ingredient.
        /// </summary>
        /// <param name="newIngredient">Ingredient to create.</param>
        public void CreateIngredient (Ingredient newIngredient)
        {
            //Convert to DO for any needed conversion logic
            var ingredient = new IngredientDO(newIngredient);
            try
            {
                using (var sqlConnection = new SqlConnection(ConnectionString))
                {
                    using (var sqlCommand = new SqlCommand("CREATE_INGREDIENT", sqlConnection))
                    {
                        //Setup stored procedure.
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.AddWithValue("@Name", ingredient.Name);
                        sqlCommand.Parameters.AddWithValue("@Unit", ingredient.Unit);
                        sqlCommand.Parameters.AddWithValue("@Price", ingredient.Price);

                        //Open connection and execute, no return to collect.
                        sqlConnection.Open();
                        sqlCommand.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                e.Data["Problem"] = "Problem creating ingredient.";
                WriteErrorToLog(e);
            }
        }

        /// <summary>
        ///     Gets list of all ingredients with details in db
        /// </summary>
        /// <returns>List of Ingredients</returns>
        public IEnumerable<Ingredient> ReadAllIngredients ()
        {
            //Return variable.
            var ingredients = new List<Ingredient>();

            try
            {
                using (var sqlConnection = new SqlConnection(ConnectionString))
                {
                    using (var sqlCommand = new SqlCommand("READ_ALL_INGREDIENTS", sqlConnection))
                    {
                        //Setup stored procedure.
                        sqlCommand.CommandType = CommandType.StoredProcedure;

                        //Open connection and collect data
                        sqlConnection.Open();
                        using (var sqlDataReader = sqlCommand.ExecuteReader())
                        {
                            while (sqlDataReader.Read())
                            {
                                ingredients.Add(
                                                new IngredientDO(
                                                                 sqlDataReader["Name"] as string,
                                                                 sqlDataReader["Unit"] as string,
                                                                 (double)sqlDataReader["Price"],
                                                                 (int)sqlDataReader["ID"]));
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.Data["Problem"] = "Problem reading all ingredients.";
                WriteErrorToLog(e);
            }

            return ingredients;
        }

        /// <summary>
        ///     Gets single ingredient by Ingredient ID with details
        /// </summary>
        /// <param name="ingredientID">Ingredient ID to get data for</param>
        /// <returns>Ingredient requested</returns>
        public Ingredient ReadIngredient (int ingredientID)
        {
            //Return variable
            IngredientDO ingredient = null;

            try
            {
                using (var sqlConnection = new SqlConnection(ConnectionString))
                {
                    using (var sqlCommand = new SqlCommand("READ_INGREDIENT", sqlConnection))
                    {
                        //Setup stored procedure.
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.AddWithValue("@ID", ingredientID);

                        //Open connection and collect return
                        sqlConnection.Open();
                        using (var sqlDataReader = sqlCommand.ExecuteReader())
                        {
                            //While loop not used since single object.
                            if (sqlDataReader.Read())
                                ingredient = new IngredientDO(
                                                              sqlDataReader["Name"] as string,
                                                              sqlDataReader["Unit"] as string,
                                                              (double)sqlDataReader["Price"],
                                                              (int)sqlDataReader["ID"]);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.Data["Problem"] = "Problem reading ingredient.";
                WriteErrorToLog(e);
            }

            return ingredient;
        }

        /// <summary>
        ///     Update ingredient details. Can change all values but Ingredient ID (primary key)
        /// </summary>
        /// <param name="ingredient">Ingredient to update</param>
        public void UpdateIngredient (Ingredient ingredient)
        {
            //Convert to DO for any needed data conversion logic
            var ingredientDO = new IngredientDO(ingredient);
            try
            {
                using (var sqlConnection = new SqlConnection(ConnectionString))
                {
                    using (var sqlCommand = new SqlCommand("UPDATE_INGREDIENT", sqlConnection))
                    {
                        //Setup stored procedure
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.AddWithValue("@ID", ingredientDO.IngredientID);
                        sqlCommand.Parameters.AddWithValue("@Name", ingredientDO.Name);
                        sqlCommand.Parameters.AddWithValue("@Unit", ingredientDO.Unit);
                        sqlCommand.Parameters.AddWithValue("@Price", ingredientDO.Price);

                        //Open connection and execute, no return to collect.
                        sqlConnection.Open();
                        sqlCommand.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                e.Data["Problem"] = "Problem updating ingredient.";
                WriteErrorToLog(e);
            }
        }

        /// <summary>
        ///     Delete ingredient by ID
        /// </summary>
        /// <param name="ingredientID">ID of ingredient to delete.</param>
        public void DeleteIngredient (int ingredientID)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(ConnectionString))
                {
                    using (var sqlCommand = new SqlCommand("DELETE_INGREDIENT", sqlConnection))
                    {
                        //Setup stored procedure
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.AddWithValue("@ID", ingredientID);

                        //Open connection and execute, no return to collect.
                        sqlConnection.Open();
                        sqlCommand.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                e.Data["Problem"] = "Problem deleting ingredient.";
                WriteErrorToLog(e);
            }
        }
    }
}