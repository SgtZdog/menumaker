﻿using MenuModels;

namespace MenuMakerDAL.Models
{
    /// <summary>
    ///     DO version of IngredientImplementation. No default constructor to force complete object.
    /// </summary>
    public class IngredientImplementationDO : IngredientImplementation
    {
        internal IngredientImplementationDO (
            int recipeId,
            double quantity,
            Ingredient ingredient,
            int implementationId)
            : base(recipeId, quantity, ingredient, implementationId) { }

        public IngredientImplementationDO (IngredientImplementation ingredientImplementation)
            : base(ingredientImplementation) { }
    }
}