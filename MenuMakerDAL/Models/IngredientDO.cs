﻿using MenuModels;

namespace MenuMakerDAL.Models
{
    /// <summary>
    ///     DO version of Ingredient. No default constructor to force complete object.
    /// </summary>
    public class IngredientDO : Ingredient
    {
        internal IngredientDO (string name, string unit, double price, int ingredientId = default(int))
            : base(name, unit, price, ingredientId) { }

        public IngredientDO (Ingredient ingredient)
            : base(ingredient) { }
    }
}