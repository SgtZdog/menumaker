﻿using MenuModels;

namespace MenuMakerDAL.Models
{
    /// <summary>
    ///     DO version of User. No default constructor to force complete object.
    /// </summary>
    public class UserDO : User
    {
        internal UserDO (int id, int roleId, string username, string password, string roleName, string roleDescription)
            : base(id, roleId, username, password, roleName, roleDescription) { }

        public UserDO (User user)
            : base(user) { }
    }
}