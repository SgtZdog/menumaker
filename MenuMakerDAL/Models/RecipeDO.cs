﻿using MenuModels;

namespace MenuMakerDAL.Models
{
    /// <summary>
    ///     DO version of Recipe. No default constructor to force complete object.
    /// </summary>
    public class RecipeDO : Recipe
    {
        internal RecipeDO (string name, string directions, string description, int id)
            : base(name, directions, description, id) { }

        public RecipeDO (Recipe recipe)
            : base(recipe) { }
    }
}