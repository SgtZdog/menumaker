﻿using System;
using System.Data;
using System.Data.SqlClient;
using MenuMakerDAL.Models;
using MenuModels;

namespace MenuMakerDAL
{
    /// <summary>
    ///     User Data Access object. No default ctor to force setting connection string.
    /// </summary>
    public class UsersDAO : DAO
    {
        public UsersDAO (string connection)
            : base(connection) { }

        /// <summary>
        ///     Creates User in DB. Used for registering a new user.
        /// </summary>
        /// <param name="userToCreate">User that will be created.</param>
        public void CreateUser (User userToCreate)
        {
            //Convert to DO for conversion logic
            var userDOToCreate = new UserDO(userToCreate);
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var command = new SqlCommand("CREATE_USER", connection))
                    {
                        //Setup stored procedure
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Username", userDOToCreate.Username);
                        command.Parameters.AddWithValue("@Password", userDOToCreate.Password);
                        command.Parameters.AddWithValue("@RoleID", userDOToCreate.RoleID);

                        //Open connection and execute, no return to collect
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                e.Data["Problem"] = "Problem creating user.";
                WriteErrorToLog(e);
            }
        }

        /// <summary>
        ///     Gets user by ID
        /// </summary>
        /// <param name="userID">ID of user</param>
        /// <returns>User requested</returns>
        public User ReadUser (int userID)
        {
            //Return variable
            UserDO user = null;
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var command = new SqlCommand("READ_USER", connection))
                    {
                        //Setup stored procedure
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", userID);

                        //Open connection and execute, collect returned user
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                                user = new UserDO(
                                                  (int)reader["ID"],
                                                  (int)reader["RoleID"],
                                                  reader["Username"] as string,
                                                  reader["Password"] as string,
                                                  reader["RoleName"] as string,
                                                  reader["Description"] as string);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.Data["Problem"] = "Problem reading user.";
                WriteErrorToLog(e);
            }

            return user;
        }

        /// <summary>
        ///     Reads user by username, used for login and search
        /// </summary>
        /// <param name="username">username of desired user</param>
        /// <returns>User that matches username</returns>
        public User ReadUserByName (string username)
        {
            //Return variable
            UserDO user = null;
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var command = new SqlCommand("READ_USER_BY_NAME", connection))
                    {
                        //Setup stored procedure
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Username", username);

                        //Open connection and execute, collect return user
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                                user = new UserDO(
                                                  (int)reader["ID"],
                                                  (int)reader["RoleID"],
                                                  reader["Username"] as string,
                                                  reader["Password"] as string,
                                                  reader["RoleName"] as string,
                                                  reader["Description"] as string);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.Data["Problem"] = "Problem reading user.";
                WriteErrorToLog(e);
            }

            return user;
        }

        /// <summary>
        ///     Update user
        /// </summary>
        /// <param name="user">User to update</param>
        public void UpdateUser (User user)
        {
            //Convert to DO for conversion logic
            var userDO = new UserDO(user);
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var command = new SqlCommand("UPDATE_USER", connection))
                    {
                        //Setup stored procedure
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", userDO.ID);
                        command.Parameters.AddWithValue("@Username", userDO.Username);
                        command.Parameters.AddWithValue("@Password", userDO.Password);
                        command.Parameters.AddWithValue("@RoleID", userDO.RoleID);

                        //Open connection and execute, no return to collect.
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                e.Data["Problem"] = "Problem updating user.";
                WriteErrorToLog(e);
            }
        }

        /// <summary>
        ///     Deletes user by ID.
        /// </summary>
        /// <param name="userID">id of user to delete.</param>
        public void DeleteUser (int userID)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var command = new SqlCommand("DELETE_USER", connection))
                    {
                        //Setup stored procedure
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", userID);

                        //Open connection and execute, no return to collect
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                e.Data["Problem"] = "Problem deleting user.";
                WriteErrorToLog(e);
            }
        }
    }
}